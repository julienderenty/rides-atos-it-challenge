# --- First database schema
 
# --- !Ups

CREATE TABLE usr (
  id                        SERIAL PRIMARY KEY,
  username                  VARCHAR(255) UNIQUE not null,
  firstname					VARCHAR(255),
  mail                      VARCHAR(255) not null,
  score						BIGINT,
  nbreviews					BIGINT,
  avatar					VARCHAR(255),
  phonenumber				VARCHAR(255),  
  pass                      VARCHAR(255) not null,
  valid_account				BOOLEAN not null
);

CREATE TABLE review (
  id                        SERIAL PRIMARY KEY,
  text                     	VARCHAR(255) not null,
  from_user					BIGINT,
  date						TIMESTAMP without time zone DEFAULT now(),
  nature					BIGINT,
  target_user				BIGINT
);


# --- !Downs

DROP TABLE IF EXISTS usr;

DROP TABLE IF EXISTS review;