import sbt._
import Keys._
import PlayProject._

object ApplicationBuild extends Build {

    val appName         = "itc"
    val appVersion      = "1.0-SNAPSHOT"

    val appDependencies = Seq(
      // Add your project dependencies here,
        "org.slf4j" % "slf4j-api" % "1.6.4",
        "postgresql" % "postgresql" % "9.1-901.jdbc4",
        "junit" % "junit" % "4.10",
        "junit" % "junit" % "4.8.1" % "test",
        "org.scalatest" %% "scalatest" % "1.7.1" % "test",
        "org.mindrot" % "jbcrypt" % "0.3m"
    )

    
    val main = PlayProject(appName, appVersion, appDependencies, mainLang = SCALA).settings(
      // Add your own project settings here      
        resolvers += "fyrie snapshots" at "http://repo.fyrie.net/snapshots"
    )

}
