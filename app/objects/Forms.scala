package objects

import utils.DoubleFormat.double
import play.api.data.Form
import play.api.data.Forms._
import anorm._
import models._
import play.api.data.format.Formats.longFormat

object Forms {
  
	object offlineForms {
		val registrationForm = Form(
		  mapping(
		    "id" -> 		ignored(NotAssigned:Pk[Long]),
		    "username" -> 	text.verifying("Must be over 2 characters",_.size>1)
		    					.verifying("Username already exists",!User.unauthorized(_))
		    					.verifying("Alphanumerics only!", _.matches("^[a-zA-Z0-9.]*/?$")),
		    "firstname" -> 	text,
		    "phonenumber" -> text,
		    "mail" -> 		text,
		    "pass" -> 		text.verifying("Password must be over 6 characters",_.size>6)
		  )
		  ((id, username, firstname, phonenumber, mail, pass) => User(id,username,firstname,mail,0,0,"",phonenumber,pass,false))
		  ((user: User) => Some(user.id, user.username, user.firstname, user.phonenumber, user.mail, user.pass))
		)
		
		val loginForm = Form(
		  tuple(
		    "username" -> 	text,
		    "pass" -> 		text
		  ) verifying ("Invalid username or password", result => result match {
		  	case (username, password) => User.exists(username,password)
		  })
		)
	}
	
}