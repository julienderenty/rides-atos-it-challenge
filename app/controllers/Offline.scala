package controllers

import play.api._
import play.api.mvc._
import models._
import objects.Forms._
import backend.Messages._
import play.api.libs.json._

object Offline extends Controller {
	
	/**
	 * Registration and logging in actions.
	 */
	def registering = Action { implicit request =>
    	offlineForms.registrationForm.bindFromRequest.fold(
    		formWithErrors => {
    			Logger.error("Impossible registration")
    			BadRequest("Impossible registration")  
    		},
    		user => {
    			Logger.info("Succesful registration " + user.username)
    			val id = User.insert(user)
    			Ok(userslist(User.getRankedUsers())).as(JSON).withSession(
					"id"-> id.get.toString,
					"username"-> user.username,
					"firstname" -> user.firstname,
					"score" -> user.score.toString,
					"avatar" -> user.avatar,
					"phonenumber" -> user.phonenumber
    			)
    		}
    	)
	}
	
	def logging = Action { implicit request =>
    	offlineForms.loginForm.bindFromRequest.fold(
    		formWithErrors => {
    			Logger.error("Impossible connection")
    			BadRequest("Impossible connection")
    		},
    		user => {
    		  val userInformation = User.getByUserName(user._1)
    		  userslist(User.getRankedUsers())
    		  Logger.info("Succesful connection " + user._1)
    		  Ok(userslist(User.getRankedUsers())).as(JSON).withSession(
					"id"-> userInformation.get.id.get.toString,
					"username"-> userInformation.get.username,
					"firstname" -> userInformation.get.firstname,
					"score" -> userInformation.get.score.toString,
					"avatar" -> userInformation.get.avatar,
					"phonenumber" -> userInformation.get.phonenumber
    			)
    		}
    	)
	}
	
	def userslist(l: List[User]) = {
		JsObject(
			Seq(
				"type" -> JsString("profileSummaries"),
				"riders" -> JsArray(
					l.map(p => {
						JsObject(
							Seq(
								"id" -> JsNumber(p.id.get),
								"firstname" -> JsString(p.firstname),
								"username" -> JsString(p.username),
								"score" -> JsNumber(p.score),
								"profilePicURL" -> JsString(p.avatar),
								"nbreviews" -> JsNumber(p.nbreviews)
							)
						)
					})
				)
			)
		)
	}
	
}