package controllers

import models._
import play.api.mvc._
import views.html
import objects.Forms._
import play.api.libs.json._
import scala.math._
import play.api.Logger
import play.api.libs.iteratee._
import play.api.libs.concurrent.Promise

object Online extends Controller {
  
	protected val Home = Redirect(routes.Application.index)
	
	/**
	 * Session related actions.
	 */
	def loggingOut = Action { implicit request => 
	  if(session.get("id") != None){
	    val id = session.get("id").get.toLong
		backendInterface.IncomingEventsInterface.isOffline(id)
		Logger.info("User " + id + " logged out")
	  }
	  Home.withNewSession
	}
	
	/**
	 * Handles rides search
	 */
	def search(originlat: Int, originlon: Int, destlat: Int, destlon: Int, nbpersonnes: Int) = Action { implicit request => 
		if(session.get("id")!=None){
			Logger.info("Searching for rides")
			val dbloriginLat = (originlat / (pow(10,6):Double)):Double
			val dbloriginLon = (originlon / (pow(10,6):Double)):Double
			val dbldestLat = (destlat / (pow(10,6):Double)):Double
			val dbldestLon = (destlon / (pow(10,6):Double)):Double
			val resultPromise = backendInterface.IncomingEventsInterface.searchRides(session.get("id").get.toLong,
																					dbloriginLat, 
																					dbloriginLon,
																					dbldestLat,
																					dbldestLon)
			Async {
		        resultPromise map({ res => 
		          Logger.info("Search results are being returned to user " + session.get("id").get)
		          Ok(res).as(JSON)
		        })
		    }
		}else{
			Logger.error("Search query not allowed")
			BadRequest("You have to be online to search for rides")
		}
	}
	
	/**
	 * Returns profile information
	 */
	def profile(id: Long) = Action { implicit request =>
		if(session.get("id")!=None){
			if(id != -1){
				val user = User.findById(id)
				if(user!=None){
					val reviews = Review.findByTargetId(id)
					val msg = JsObject(
						Seq(
							"id" -> JsNumber(id),
							"username" -> JsString(user.get.username),
							"name" -> JsString(user.get.firstname),
							"score" -> JsNumber(user.get.score),
							"phonenumber" -> JsString(user.get.phonenumber),
							"avatar" -> JsString(user.get.avatar),
							"reviews" -> JsArray(
					          reviews.map({
					            p => {
					              JsObject(
					            	Seq(
					            	    "text" -> JsString(p._1.text),
					            	    "nature" -> JsNumber(p._1.nature),
					            	    "date" -> JsNumber(p._1.date.getTime),
					            	    "reviewerusername" -> JsString(p._2.getOrElse(User.nullUser).username),
					            	    "reviewername" -> JsString(p._2.getOrElse(User.nullUser).firstname),
					            	    "revieweravatar" -> JsString(p._2.getOrElse(User.nullUser).avatar),
					            	    "reviewerscore" -> JsNumber(p._2.getOrElse(User.nullUser).score)
					            	)
					              )
					            }
					          })
					        )
						)
					)
					Ok(msg).as(JSON)
				}else{
					NotFound("Not found user " + id)
				}
			}else{
				BadRequest("Please specify and id")
			}
		}else{
			BadRequest("Permission denied for profile request")
		}
	}
	
	
	def testSocket = Action { implicit request => 
	  Ok(views.html.testSocket())
	}
	
	/**
	 * Handles websocket connection.
	 */
	def connect = WebSocket.async[JsValue] { implicit request  =>
	  	if(session.get("id")!=None){
	  		val id = request.session.get("id").get.toLong
		  	Logger.info("User " + id + " is getting connected via WebSocket...")
			backendInterface.IncomingEventsInterface.isOnline(id)
	  	}else{
	  		Logger.error("Connect Action: Websocket connection not allowed")
	  		val iteratee = Done[JsValue,Unit]((),Input.EOF)
			val enumerator =  Enumerator[JsValue](JsObject(Seq("error" -> JsString("Websocket connection not allowed, you have to be online!")))).andThen(Enumerator.enumInput(Input.EOF))
			(iteratee,enumerator).asInstanceOf[Promise[(Iteratee[JsValue,_],Enumerator[JsValue])]]
	  	}
	}
	
}