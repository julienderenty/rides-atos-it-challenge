package controllers

import play.api._
import play.api.mvc._

object Application extends Controller {
  
  def index = Action { implicit request =>
    if(session.get("id")==None){
    	Ok(views.html.index(""))
    }else{
    	Ok("You are online!")
    }
  }
  
  def testCloudFront = Action { implicit request =>
    Ok(views.html.testCloudFront())
  }
  
}