package backendInterface

import backend.Messages._
import akka.actor._
import akka.pattern._
import akka.util.Timeout
import akka.util.duration._
import play.api.libs.json._
import play.api.libs.iteratee._
import play.api.libs.concurrent._
import play.api.Play.current
import java.util.Date
import scala.math._
import java.util.Calendar
import java.util.TimeZone
import backend._
import Messages._
import backend.Actors._
import Configuration._
import play.api._

object IncomingEventsInterface {
	
  	/**
	 * Called when a user manually logs out.
	 */
	def isOffline(terminalId:Long) = {
		OutgoingEventsInterface.outgoingEventsHandler ! LoggedOut(terminalId)
	}
	/**
	 * Called when a user logs in when a new tab is opened,
	 * TabsHandling keeps track of it
	 */
	def isOnline(uid:Long):Promise[(Iteratee[JsValue,_],Enumerator[JsValue])] = {
		(OutgoingEventsInterface.outgoingEventsHandler ? LoggedIn(uid)).asPromise.map {
			case Connected(enumerator) => {
				val iteratee = Iteratee.foreach[JsValue] { event => {
					if((event \ "type").as[String] == "close"){
						Logger.info("User " + uid + " closed their websocket connection via close message")
						OutgoingEventsInterface.outgoingEventsHandler ! CloseConnection(uid)
					}else{
						Logger.info("User " + uid + " sent " + event + " and it is being handled")
						incomingEventsHandler ! IncomingEvent(uid,event)
					}
				}}
				(iteratee,enumerator)
			}
			case CannotConnect(error) => {
	  	  		Logger.error("Websocket connection not allowed")
				val iteratee = Done[JsValue,Unit]((),Input.EOF)
				val enumerator =  Enumerator[JsValue](JsObject(Seq("error" -> JsString(error)))).andThen(Enumerator.enumInput(Input.EOF))
				(iteratee,enumerator)
			}
		}
	}
  
	val incomingEventsHandler = system.actorOf(Props(new IncomingEventsHandler), "incomingEventsHandler")
	
	class IncomingEventsHandler extends Actor {
		def receive = {
		  	case IncomingEvent(uid,event) => handleEvent(uid,event)
		}
	}
	
  	/**
  	 * Translates a message from Json form
  	 * to an actual case class message
  	 * by calling the methods bellow
  	 */
	def handleEvent(uid: Long,event: JsValue) = {
	  (event \ "type").as[String] match {
	    case "updatePosition" => {
	      updatePosition(
	          uid,
	          (((event \ "lat").as[Int].toDouble) / (pow(10,6):Double)):Double,
	          (((event \ "lon").as[Int].toDouble) / (pow(10,6):Double)):Double
	      )
	    }
	    case "storeRide" => {
	      storeDrive(
	          uid,
    		  (((event \ "originLat").as[Int].toDouble) / (pow(10,6):Double)):Double,
    		  (((event \ "originLon").as[Int].toDouble) / (pow(10,6):Double)):Double,
    		  (((event \ "destLat").as[Int].toDouble) / (pow(10,6):Double)):Double,
    		  (((event \ "destLon").as[Int].toDouble) / (pow(10,6):Double)):Double,
    		  new Date((event \ "arrivalDate").as[Long]),
    		  (event \ "nbPlaces").as[Int],
    		  (event \ "waypoints").as[Seq[play.api.libs.json.JsValue]].map{
	            p => (
	            		((p \ ("lat")).as[Int].toDouble) / (pow(10,6):Double):Double,
	            		((p \ ("lon")).as[Int].toDouble) / (pow(10,6):Double):Double
	            	)
	          }.toList
	      )
	    }
	    case "askForRide" => {
	      askForRide(
	          uid,
	          (event \ "offereruid").as[Long],
	          (((event \ "lat").as[Int].toDouble) / (pow(10,6):Double)):Double,
    		  (((event \ "lon").as[Int].toDouble) / (pow(10,6):Double)):Double
	      )
	    }
	    case "choiceFromDriver" => {
	      val choice = (event \ "choiceFromDriver").as[Int]
	      choice match {
	        case 0 => refuseAsker(uid,(event \ "askeruid").as[Long])
	        case 1 => acceptAsker(uid,(event \ "askeruid").as[Long])
	        case 2 => refuseAsker(uid,(event \ "askeruid").as[Long])
	      }
	    }
	  }
  	}
	
	/**
	 * Json
	 * To be called to enter a ride into the system
	 */
	def storeDrive(uid:Long,
					originLat:Double,
					originLon:Double,
					destLat:Double,
					destLon:Double,
					arrivalDate:Date,
					nbPlaces:Int,
					waypoints:List[(Double,Double)]) = {
		onlineUserMaster ! StoreRide(uid,originLat,originLon,destLat,destLon,arrivalDate,nbPlaces,waypoints)
	}
	private def getIntHour(dateDate: Date) = {
	  val cal = Calendar.getInstance(TimeZone.getTimeZone("GMT+1"))
	  cal.setTime(dateDate)
	  cal.get(Calendar.HOUR_OF_DAY) * 100 + cal.get(Calendar.MINUTE)
	}
	
	/**
	 * Json
	 * To be called when a user selects a ride
	 */
	def askForRide(askeruid:Long,offereruid:Long,askerLat:Double,askerLon:Double) = {
		onlineUserMaster ! AskerAtAsker(askeruid,offereruid,askerLat,askerLon)
	}
	
	/**
	 * Json
	 * To be called when a user accepts a rider
	 */
	def acceptAsker(offereruid:Long,askeruid:Long) = {
		onlineUserMaster ! AcceptedAskerOfferer(offereruid,askeruid)
	}
	
	/**
	 * Json
	 * To be called when a user refuses a rider
	 */
	def refuseAsker(offereruid:Long,askeruid:Long) = {
		onlineUserMaster ! RefusedAskerOfferer(offereruid,askeruid)
	}
	
	/**
	 * Json
	 * To be called when a position event is received
	 */
	def updatePosition(uid:Long,lat:Double,lon:Double) = {
		onlineUserMaster ! UpdatedPosition(uid:Long,lat:Double,lon:Double)
	}
	
	/**
	 * HTTP Request
	 * Search for rides with destination nearby
	 */
	def searchRides(uid:Long,originLat:Double,originLon:Double,destLat:Double,destLon:Double): Promise[JsObject] = {
		backend.SearchHandler.searchTrigger(uid, originLat, originLon, destLat, destLon)
	}
}