package backendInterface

import play.api.libs.json.JsObject
import play.api.libs.iteratee.PushEnumerator
import play.api.libs.json._
import backend._
import Configuration._
import Messages._
import backend.Actors._
import akka.actor._
import akka.pattern._
import akka.util.Timeout
import play.api.libs.iteratee._
import akka.util.duration._
import play.api.libs.concurrent._
import play.api.Play.current
import play.api.Logger

object OutgoingEventsInterface {
	
	val outgoingEventsHandler = system.actorOf(Props(new OutgoingEventsHandler), "outgoingEventsHandler")
	
	class OutgoingEventsHandler extends Actor {
	  	var channels = Map[Long,PushEnumerator[JsValue]]()
		def receive = {
	  	  	case msg: OnlineUserToOutgoingEventsHandler => {
	  	  	  if(channels.contains(msg.uid)){
	  	  	    handleMessage(channels(msg.uid),msg)
	  	  	  }
	  	  	}
			case LoggedIn(uid) => {
				if(channels.contains(uid)){
					sender ! Connected(channels(uid))
				}else{
					var channel =  Enumerator.imperative[JsValue]()
					channels = channels + (uid -> channel)
					sender ! Connected(channels(uid))
					onlineUserMaster ! CreateOnlineUser(uid)
				}
			}
			case LoggedOut(uid) => {
				channels -= uid
				onlineUserMaster ! KillOnlineUser(uid)
			}
		}
	}
	
	/**
	 * Handles a message coming from backend
	 * pushes the correspondant json to the
	 * appropriate user
	 */
	def handleMessage(channel:PushEnumerator[JsValue],msg:OnlineUserToOutgoingEventsHandler) = {
		msg match {
		  case UpdatePositionTic(userId,pinId,lat,lon) => {
			val msg = JsObject(
				Seq(
					"type" -> JsString("locationUpdate"),
					"userId" -> JsNumber(pinId),
					"lat" -> JsNumber(lat),
					"lon" -> JsNumber(lon)
				)
			)
			Logger.info("Update position tic is being pushed to terminal " + userId)
			channel.push(msg)
		  }
		  case NotificationAskerAt(userId,askerId,askerLat,askerLon) => {
	        val asker = models.User.findById(askerId).getOrElse(models.User.nullUser)
	        val msg = JsObject(
				Seq(
					"type" -> JsString("newAskerNotification"),
					"askerId" -> JsNumber(askerId),
					"askerName" -> JsString(asker.firstname),
					"askerUsername" -> JsString(asker.username),
					"askerScore" -> JsNumber(asker.score),
					"askerPhoneNumber" -> JsString(asker.phonenumber),
					"askerProfilePicURL" -> JsString(asker.avatar),
					"askerNbreviews" -> JsNumber(asker.nbreviews),
					"lat" -> JsNumber(askerLat),
					"lon" -> JsNumber(askerLon)
				)
			)
			Logger.info("Asker notification is being pushed to terminal " + userId)
			channel.push(msg)
		  }
		  case NotificationAccepted(userId,offererId) => {
		    backend.SearchHandler.getRide(userId).onSuccess({
		      case ride => {
		        val offerer = models.User.findById(offererId).getOrElse(models.User.nullUser)
		    	val msg = JsObject(
					Seq(
						"type" -> JsString("acceptedRideNotification"),
						"offererId" -> JsNumber(offererId),
						"offererName" -> JsString(offerer.firstname),
						"offererUsername" -> JsString(offerer.username),
						"offererScore" -> JsNumber(offerer.score),
						"offererPhoneNumber" -> JsString(offerer.phonenumber),
						"offererProfilePicURL" -> JsString(offerer.avatar),
						"offererNbreviews" -> JsNumber(offerer.nbreviews),
						"originLat" -> JsNumber(ride._2),
						"originLon" -> JsNumber(ride._3),
						"destLat" -> JsNumber(ride._4),
						"destLon" -> JsNumber(ride._5),
						"arrivalDate" -> JsNumber(ride._6),
						"waypoints" -> JsArray(
				          ride._8.toList.map({
				            p => {
				              JsObject(
				            	Seq(
				            	    "lat" -> JsNumber(p._1),
				            	    "lon" -> JsNumber(p._2)
				            	)
				              )
				            }
				          })
				        )
					)
				)
				Logger.info("Accepted notification with ride are being pushed to terminal " + userId)
				channel.push(msg)
		      }
		    })
		  }
		  case NotificationRefused(userId,offererId) => {
		    val msg = JsObject(
				Seq(
					"type" -> JsString("refusedRideNotification"),
					"offererId" -> JsNumber(offererId)
				)
			)
			Logger.info("Refused notification is being pushed to terminal " + userId)
			channel.push(msg)
		  }
		  case _ => 
		}
	}
	
}