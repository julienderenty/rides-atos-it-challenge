package models

import play.api.db._
import play.api.Play.current
import anorm._
import anorm.SqlParser._
import java.util.Date

case class Review(id: Pk[Long],
    text: String,
    from_user: Long,
    date: Date,
    nature: Int,
    target_user: Long)

object Review {

	/**
	 * Parse a Review from a ResultSet
	 */
	val simple = {
		get[Pk[Long]]("review.id") ~
		get[String]("review.text") ~
		get[Long]("review.from_user") ~
		get[Date]("review.date") ~
		get[Int]("review.nature") ~
		get[Long]("review.target_user") map {
			case id~
				text~
				from_user~
				date~
				nature~
				target_user => Review(id,
									text,
									from_user,
									date,
									nature,
									target_user)
		}
	}
	
	/**
	 * Parse a (Article,User) from a ResultSet
	 */
	val withUser = Review.simple ~ (User.simple ?) map {
    	case review~user => (review,user)
	}
	
	/**
	 * Insert a Review in the database
	 */
	def insert(review: Review) = {
		DB.withConnection { implicit connection =>
			SQL("""insert into review(text,from_user,nature,target_user) 
								values 
								({text},
								{from_user},
								{nature},
							    {target_user})""").on(
				'text -> review.text,
				'from_user -> review.from_user,
				'nature -> review.nature,
				'target_user -> review.target_user
			).executeInsert()
			SQL("""update usr set nbreviews=nbreviews+1 where id={id}""").on(
		    	'id -> review.target_user
		    ).executeUpdate()
		}
	}
  
	/**
	 * Retrieve all reviews about a user
	 */
	def findByTargetId(target_user: Long) = {
		val result = DB.withConnection { implicit connection =>
		  	val user = SQL("select * from usr where id = {target_user}").on('target_user -> target_user).as(User.simple.singleOpt)
		  	if(user!=None){
				val reviews = SQL("""
				    select * from review 
				    left join usr on review.from_user = usr.id
				    where review.target_user = {target_user}
				    order by date desc nulls last
				    """).on(
				        'target_user -> user.get.id
				    ).as(Review.withUser *)
				reviews
		  	}else{
		  	  List[(Review,Option[User])]()
		  	}
		}
		result
	}
	
}