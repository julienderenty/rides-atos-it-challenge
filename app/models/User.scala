package models

import play.api.db._
import play.api.Play.current

import anorm._
import anorm.SqlParser._

case class User(id: Pk[Long],
    username: String,
    firstname: String,
    mail: String, 
    score: Long,
    nbreviews: Long,
    avatar: String,
    phonenumber: String,
    pass: String,
    valid_account: Boolean)

object User {
  
	val nullUser = User(null,"Deleted Account","Deleted Account","Deleted Account",-1,-1,"Deleted Account","Deleted Account","Deleted Account",false)
  
  	/**
	 * Parse a User from a ResultSet
	 */
	val simple = {
		get[Pk[Long]]("usr.id") ~
		get[String]("usr.username") ~
		get[String]("usr.firstname") ~
		get[String]("usr.mail") ~
		get[Long]("usr.score") ~
		get[Long]("usr.nbreviews") ~
		get[String]("usr.avatar") ~
		get[String]("usr.phonenumber") ~
		get[String]("usr.pass") ~
		get[Boolean]("usr.valid_account") map {
			case id~
				username~
				firstname~
				mail~
				score~
				nbreviews~
				avatar~
				phonenumber~
				pass~
				valid_account => User(id,
									username,
									firstname,
									mail,
									score,
									nbreviews,
									avatar,
									phonenumber,
									pass,
									valid_account)
		}
	}
	
	/**
	 * Insert a User in the database
	 */
	def insert(user: User) = {
		DB.withConnection { implicit connection =>
			SQL("""insert into usr(username,firstname,mail,score,nbreviews,avatar,phonenumber,pass,valid_account) 
								values 
								({username},
								{firstname},
							    {mail},
							    {score},
								{nbreviews},
							    {avatar},
							    {phonenumber},
							    {pass},
								{valid_account})""").on(
				'username -> user.username,
				'firstname -> user.firstname,
				'mail -> user.mail,
				'score -> user.score,
				'nbreviews -> user.nbreviews,
				'avatar -> user.avatar,
				'phonenumber -> user.phonenumber,
				'pass -> hashPassword(user.pass),
				'valid_account -> user.valid_account
			).executeInsert()
		}
	}
	
	/**
	 * Returns ranked list of users
	 */
	def getRankedUsers() = {
		DB.withConnection { implicit connection =>
			SQL("select * from usr order by score desc nulls last").as(User.simple *)
		}
	}
	
	/**
	 * Retrieve a user from the id
	 */
	def findById(id: Long): Option[User] = {
		DB.withConnection { implicit connection =>
			SQL("select * from usr where id = {id}").on('id -> id).as(User.simple.singleOpt)
		}
	}
	
	/**
	 * Retrieve a user from the id
	 */
	def getByUserName(username: String): Option[User] = {
		DB.withConnection { implicit connection =>
			SQL("select * from usr where username = {username}").on('username -> username).as(User.simple.singleOpt)
		}
	}
	
	/**
	 * Checks if username authorized
	 */
	def unauthorized(username: String): Boolean = {
		if(username=="events" ||
			username=="edit" ||
			username=="logout" ||
			username=="registering" ||
			username=="publishing" ||
			username=="logging" ||
			username=="notfound" ||
			username=="assets" ||
			!(username.matches("^[a-zA-Z0-9.]*$"))
		){
			true
		}else{
			val a = DB.withConnection { implicit connection =>
				SQL("select * from usr where username = {username}").on('username -> username).as(User.simple.singleOpt)
			}
			if(a!=None)
			  true
			else
			  false
		}
	}
	
	/**
	 * Check if a user is in the db
	 */
	def exists(username: String, password: String) = {
		val a = DB.withConnection { implicit connection =>
		  	SQL("select * from usr where username={username}").on('username -> username).as(User.simple.singleOpt)
		}
		if(a!=None && matchPasswords(password,a.get.pass))
			true
		else
			false
	}
	
	def update(id: Long, username: String, mail: String) = {
		DB.withConnection { implicit connection =>
		  val a = SQL("""update usr set username={username},
				  						mail={mail}
				  						where id={id}""").on(
		    	'username -> username,
		    	'mail -> mail,
		    	'id -> id
		    ).executeUpdate()
		}
	}
	
	private def hashPassword(password: String) = {
        password
    }
	
	private def matchPasswords(providedPassword: String, storedPassword: String) = providedPassword.equals(storedPassword)
	
}