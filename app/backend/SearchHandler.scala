package backend

import akka.actor._
import Configuration._
import play.api.libs.json._
import scala.collection.mutable.ListBuffer
import scala.collection.immutable._
import java.util.TimeZone
import java.util.Calendar
import net.fyrie.redis._
import scala.math._
import java.util.Date
import akka.pattern._
import Messages._
import play.api.libs.concurrent._
import akka.dispatch.Future

object SearchHandler {
  import DistanceModule._

	/**
	 * Asynchronous search trigger
	 */
	def searchTrigger(uid:Long,originLat:Double,originLon:Double,destLat:Double,destLon:Double): Promise[JsObject] = {
	  (searchHandler ? SearchForRides(uid,originLat,originLon,destLat,destLon)).asPromise.map {
		  case search: SearchJsonMsg => search.msg
		  case _ => JsObject(Seq())
	  }
  	}
	
  	/**
  	 * Returns a Future tuple describing
  	 * the ride offered by user with
  	 * id = ride
  	 */
  	def getRide(ride: Long) = {
		val resultHash = redisClient.hmget("ride:" + ride, Seq("originLat",
																"originLon",
																"destLat",
																"destLon",
																"arrivalDate",
																"nbPlaces")).parse[Double,
																                   Double,
																                   Double,
																                   Double,
																                   Int,
																                   Int]
		val listRes = redisClient.lrange("ride:"+ ride + ":waypoints", 0, -1).parse[String]
		val f = for{
		  redisWaypoints <- listRes
		  result <- resultHash
		  if !result.isEmpty
		} yield {
		  val waypoints = ListBuffer[(Double,Double)]()
		  redisWaypoints.getOrElse(List()).map({
		    redisWaypoint => waypoints += retrieve(redisWaypoint)
		  })
		  ((ride,result(0)._1.get,result(0)._2.get,result(0)._3.get,result(0)._4.get,result(0)._5.get,result(0)._6.get,waypoints.toList))
		}
		f
  	}
  
  	/**
  	 * Search handling all search requests
  	 * Checks all the rides, sorts the "good"
  	 * rides by order of priority using an algorithm
  	 * based on three critereas:
  	 * Arrival date(2), distance to destination(1) and
  	 * distance to ride(1)
  	 * Sends back the result
  	 */
  	val searchHandler = system.actorOf(Props(new Search), "searchHandler")
	class Search extends Actor {
		def receive = {
			case SearchForRides(uid,originLat,originLon,destLat,destLon) => {
			  lookupRides.map({
				  case rides => {
				    var bufferedRides = ListBuffer[(RideRow,Double,Double)]()
		    		var maxDistancesToRide = 0.0
				    // Look for rides going to more or less the same destination
					for(ride <- rides){
						// Check if ride and asker have more or less the same destination
						val distanceToDestination = distanceInMeters(Point(ride._4,ride._5),Point(destLat,destLon))
						if(distanceToDestination < AUTHORIZED_DIFFERENCE){
							// Stores the value of the distance that the asker has to walk to join the ride
							var minDistanceToRide = 0.0
							// For each of the waypoints
							for(point <- ride._8){
								val tmp = distanceInMeters(Point(point._1,point._2),Point(originLat,originLon))
								// If distance to that point is less than the minDistanceToRide
								// Change minDistanceToRide
								if(tmp < minDistanceToRide | minDistanceToRide.equals(0.0)){
									minDistanceToRide = tmp
								}
							}
							// Stores max distances to all rides
							// Used to give each ride a score
							if(minDistanceToRide > maxDistancesToRide){
								maxDistancesToRide = minDistanceToRide
							}
							bufferedRides += ((ride,minDistanceToRide,distanceToDestination))
						}
					}
					// Once the buffer is filled, sort this rides by accuracy
					val sortedRides = sort(maxDistancesToRide,destLat,destLon,originLat,originLon,bufferedRides)
					val a = JsObject(
						Seq(
							"type" -> JsString("SearchResult"),
							"rides" -> JsArray(
								sortedRides.map(p => {
									val offerer = models.User.findById(p._1._1).getOrElse(models.User.nullUser)
									JsObject(
										Seq(
											"distanceToRide" -> JsNumber(p._2),
											"distanceToDestination" -> JsNumber(p._3),
											"arrivalDate" -> JsNumber(p._1._6),
											"offererId" -> JsNumber(p._1._1),
											"offererName" -> JsString(offerer.firstname),
											"offererUsername" -> JsString(offerer.username),
											"offererScore" -> JsNumber(offerer.score),
											"offererProfilePicURL" -> JsString(offerer.avatar),
											"offererNbreviews" -> JsNumber(offerer.nbreviews),
											"originLat" -> JsNumber((p._1._2*pow(10,6)).toInt),
											"originLon" -> JsNumber((p._1._3*pow(10,6)).toInt),
											"destLat" -> JsNumber((p._1._4*pow(10,6)).toInt),
											"destLon" -> JsNumber((p._1._5*pow(10,6)).toInt),
											"waypoints" -> JsArray(
												p._1._8.map(q => {
												  JsObject(
												      Seq(
												          "lat" -> JsNumber((q._1*pow(10,6)).toInt),
												          "lon" -> JsNumber((q._2*pow(10,6)).toInt)
												      )
												  )
												})
											)
										)
									)
								})
							)
						)
					)
					SearchJsonMsg(a)
				  }
				}).pipeTo(sender)
			}
		}
	}

  	type RideRow = (Long,Double,Double,Double,Double,Long,Int,List[(Double,Double)])
  	
  	/**
  	 * Used to store and compare two rides
  	 */
	val calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT+1"))
	val AUTHORIZED_DIFFERENCE = 200
	val SCORE_SCALE = 10
	val index = scala.collection.mutable.Map[Long,Long]()

	/**
	 * Returns a waypoint as tuple (double,double)
	 * out of a String:String
	 */
	private def retrieve(waypoint: String) = {
		val list = waypoint.split(":")
		(list(0).toDouble,list(1).toDouble)
	}

  	/**
  	 * Returns the list of all the stored rides
  	 */
	private def lookupRides = {
		val res = ListBuffer[Future[RideRow]]()
		val a = for{
		  ride <- index
		}yield{
			val resultHash = redisClient.hmget("ride:" + ride._1, Seq("originLat",
																	"originLon",
																	"destLat",
																	"destLon",
																	"arrivalDate",
																	"nbPlaces")).parse[Double,
																	                   Double,
																	                   Double,
																	                   Double,
																	                   Long,
																	                   Int]
			val listRes = redisClient.lrange("ride:"+ ride._1 + ":waypoints", 0, -1).parse[String]
			val f = for{
			  redisWaypoints <- listRes
			  result <- resultHash
			  if !result.isEmpty
			} yield {
			  val waypoints = ListBuffer[(Double,Double)]()
			  redisWaypoints.getOrElse(List()).map({
			    redisWaypoint => waypoints += retrieve(redisWaypoint)
			  })
			  ((ride._1,result(0)._1.get,result(0)._2.get,result(0)._3.get,result(0)._4.get,result(0)._5.get,result(0)._6.get,waypoints.toList))
			}
			f
		}
		Future.sequence(a)
	}
	
	/**
	 * Add a ride's id and arrival date
	 * to rides index
	 */
	def addToRides(userId:Long,arrivalDate:Long) = {
		if(!index.contains(userId)){
		  index += (userId -> arrivalDate)
		  false
		}else{
		  true
		}
	}
	
	def removeFromIndex(id: Long) = {
	  index -= id
	}
	
	/**
	 * Orders rows to return rides
	 * ordered by 
	 * Arrival date first
	 * Destination closeness second
	 * Closeness to ride third
	 */
	private def sort(maxDistancesToRide:Double,destLat:Double,destLon:Double,curLat:Double,curLon:Double,rows:ListBuffer[(RideRow,Double,Double)]) = {
		val currentTime = calendar.getTime.getTime
		var sortedRows = rows.sortWith((a,b) => compare(maxDistancesToRide,currentTime,destLat,destLon,curLat,curLon,a,b))
		sortedRows
	}

	/**
	 * Compares two rides based on
	 * their scores vis-à-vis the asker's
	 * position and the current date
	 */
	private def compare(maxDistancesToRide:Double,currentTime:Long,destLat:Double,destLon:Double,curLat:Double,curLon:Double,r1:(RideRow,Double,Double),r2:(RideRow,Double,Double)) = {
		val v1 = dateValue(r1._1._6,currentTime) + destDistValue(r1._3) + distRideValue(r1._2,maxDistancesToRide)
		val v2 = dateValue(r2._1._6,currentTime) + destDistValue(r2._3) + distRideValue(r2._2,maxDistancesToRide)
		(v1/6:Double) > (v2/6:Double)
	}

	/**
	 * Returns the difference between
	 * two dates in minutes
	 */
	def getDiffInSeconds(date:Long,currentTime:Long) = {
		(date - currentTime)/1000
	}

	/**
	 * Returns the score of a ride based 
	 * on its arrival date
	 */
	private def dateValue(time:Long,currentTime:Long) = {
		val toScale = ((getDiffInSeconds(time,currentTime)*SCORE_SCALE)/(86340:Double)):Double
		if(toScale>0){
			3*(1/toScale):Double
		}else{
			3*SCORE_SCALE:Double
		}
	}

	/**
	 * Returns the score of a ride based
	 * on the distance between its
	 * destination and the asker's destination
	 */
	private def destDistValue(distanceToDestination:Double) = {
		val toScale = ((distanceToDestination*SCORE_SCALE)/(AUTHORIZED_DIFFERENCE:Double)):Double
		if(toScale>0){
			2*(1/toScale):Double
		}else{
			2*SCORE_SCALE:Double
		}
	}

	/**
	 * Returns the score of a ride based
	 * on the minimum distance between
	 * the ride and the asker's position
	 */
	private def distRideValue(distanceToRide:Double,maxDistancesToRide:Double) = {
		val toScale = ((distanceToRide*SCORE_SCALE)/(maxDistancesToRide:Double)):Double
		if(toScale>0){
			(1/toScale):Double
		}else{
			SCORE_SCALE:Double
		}
	}
	
}