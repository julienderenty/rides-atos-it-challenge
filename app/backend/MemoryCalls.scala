package backend

import play.api.libs.json._
import scala.collection.mutable.ListBuffer
import java.util.Calendar
import java.util.TimeZone
import scala.math._
import Configuration._
import akka.util.Timeout
import net.fyrie.redis._
import java.util.Date
import play.api.Logger

trait MemoryCalls {
  import DistanceModule._
  val calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT+1"))
  
    type RideRow = (Long,Double,Double,Double,Double,Int,Int,List[(Double,Double)])
    val AUTHORIZED_DIFFERENCE = 200
    val SCORE_SCALE = 10
    val testCalendar = Calendar.getInstance(TimeZone.getTimeZone("GMT+1"))
    
    
    /**
     * For test purposes
     * comment when working in prod mode
     */
    // Ride from Mediateque to capitole
    testCalendar.set(2013, 3, 7, 21, 00, 00)
    storeInRedis(1,43.6087,1.4536,43.60452,1.44422,testCalendar.getTime.getTime,2,List[(Double,Double)]((43.60498,1.4447),
    																			(43.6048,1.4468),
                                                                                (43.6066,1.45),
                                                                                (43.6087,1.4536)))
    // Same ride arriving 5 min early
    testCalendar.set(2013, 3, 7, 20, 50, 00)
    storeInRedis(2,43.6087,1.4536,43.60452,1.44422,testCalendar.getTime.getTime,2,List[(Double,Double)]((43.60498,1.4447),
                                                                                (43.6048,1.4468),
                                                                                (43.6066,1.45),
                                                                                (43.6087,1.4536)))
    // Ride from pont neuf to the post office near capitole
    testCalendar.set(2013, 3, 7, 20, 50, 00)
    storeInRedis(3,43.6087,1.4536,43.60498,1.4447,testCalendar.getTime.getTime,2,List[(Double,Double)]((43.6048,1.4468),
                                                                               (43.6034,1.4476),
                                                                               (43.60051,1.44678),
                                                                               (43.6,1.45)))
    Logger.info("Testing rides have been stored in Redis")

    /**
     * Handles Redis operations
     */
    // TODO: Volume du coffre (petit, moyen, grand, aucun)
    def storeInRedis(userId:Long,
                    originLat:Double,
                    originLon:Double,
                    destLat:Double,
                    destLon:Double,
                    arrivalDate:Long,
                    nbPlaces:Int,
                    wayPoints:List[(Double,Double)]) = {
        val currentTime = calendar.getTime.getTime
        val expiry = SearchHandler.getDiffInSeconds(arrivalDate,currentTime)
        val exists = SearchHandler.addToRides(userId,arrivalDate)
    	redisClient.quiet.del("ride:"+ userId +":waypoints")
    	redisClient.quiet.del("ride:"+ userId)
        redisClient.quiet.hmset("ride:"+userId, Map("originLat" -> originLat.toString,
                                                    "originLon" -> originLon.toString,
                                                    "destLat" -> destLat.toString,
                                                    "destLon" -> destLon.toString,
                                                    "arrivalDate" -> arrivalDate.toString,
                                                    "nbPlaces" -> nbPlaces.toString
                                                    ))
    	wayPoints.map({
          elem => redisClient.quiet.rpush("ride:"+ userId +":waypoints",elem._1+":"+elem._2)
        })
        redisClient.quiet.expire("ride:" + userId, expiry)
        redisClient.quiet.expire("ride:"+ userId +":waypoints", expiry)
    }
  
  	def removeFromRedis(id:Long) = {
  	  	redisClient.quiet.del("ride:"+ id +":waypoints")
    	redisClient.quiet.del("ride:"+ id)
  	}
  
}