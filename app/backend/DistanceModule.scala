package backend

import scala.math._

object DistanceModule {

	/**
	 * Constants used to compute distance
	 */
	private val equatorRadius: Double = 6378137
	private val poleRadius = 6356752.3142
	private val f = 1/298.257223563
	private val degToRad = 0.0174532925199433
	private val EPSILON = 1e-12
  
	/**
	 * Defines a point using latitude
	 * and longitude coordinates
	 */
  	case class Point(lat:Double,lon:Double)
  
  	/**
  	 * Returns distance in meters
  	 * between two Points
  	 */
  	def distanceInMeters(foo:Point,bar:Point) = {
		val L = (bar.lon - foo.lon) * degToRad
		val U1 = atan((1 - f) * tan(foo.lat * degToRad))
		val U2 = atan((1 - f) * tan(bar.lat * degToRad))
		val sinU1 = sin(U1) 
		val cosU1 = cos(U1)
		val sinU2 = sin(U2)
		val cosU2 = cos(U2)
		var result: Double = -1
		var cosSqAlpha: Double = 0
		var sinSigma: Double = 0
		var cos2SigmaM: Double = 0
		var cosSigma: Double = 0
		var sigma: Double = 0
		var lambda = L
		var lambdaP: Double = 0
		var iterLimit = 20
		do {
			val sinLambda = sin(lambda)
			val cosLambda = cos(lambda)
			sinSigma = sqrt((cosU2 * sinLambda) * (cosU2 * sinLambda) + (cosU1 * sinU2 - sinU1 * cosU2 * cosLambda) * (cosU1 * sinU2 - sinU1 * cosU2 * cosLambda))
			if (sinSigma == 0) {
				result = 0
				iterLimit = 0
			}else{
				cosSigma = sinU1 * sinU2 + cosU1 * cosU2 * cosLambda
				sigma = atan2(sinSigma, cosSigma)
				val sinAlpha = cosU1 * cosU2 * sinLambda / sinSigma
				cosSqAlpha = 1 - sinAlpha * sinAlpha
				cos2SigmaM = cosSigma - 2 * sinU1 * sinU2 / cosSqAlpha
				if (cos2SigmaM == Double.NaN) {
					cos2SigmaM = 0
				}
				val C = f / 16 * cosSqAlpha * (4 + f * (4 - 3 * cosSqAlpha))
				lambdaP = lambda
				lambda = L + (1 - C) * f * sinAlpha * (sigma + C * sinSigma * (cos2SigmaM + C * cosSigma * (-1 + 2 * cos2SigmaM * cos2SigmaM)))
				iterLimit -= 1
			}
		} while (abs(lambda - lambdaP) > EPSILON && iterLimit > 0);
		if(result == -1){
			if (iterLimit == 0) {
				result = Double.NaN
			}
			val uSquared = cosSqAlpha * (equatorRadius * equatorRadius - poleRadius * poleRadius) / (poleRadius * poleRadius)
			val A = 1 + uSquared / 16384 * (4096 + uSquared * (-768 + uSquared * (320 - 175 * uSquared)))
			val B = uSquared / 1024 * (256 + uSquared * (-128 + uSquared * (74 - 47 * uSquared)))
			val deltaSigma = B * sinSigma * (cos2SigmaM + B / 4 * (cosSigma * (-1 + 2 * cos2SigmaM * cos2SigmaM) - B / 6 * cos2SigmaM * (-3 + 4 * sinSigma * sinSigma) * (-3 + 4 * cos2SigmaM * cos2SigmaM)))
			result = poleRadius * A * (sigma - deltaSigma)
		}
		result
  	}
  	
}