package backend

import Messages._
import akka.actor.Actor
import OnlineUsersHandler._
import play.api.libs.json._
import play.api._
import scala.math._

object OnlineUserTraits {
  
	/**
	 * Handles the offerer aspect of an online user, to be mixed-in with OnlineUser
	 */
	trait Offerer extends UserType {
	  subj: OnlineUser => 
		var placesLeft: Int = -1
		var flagNotify = true
		var notificationReceived = Map[Long,AskerNotification]()
		abstract override def receive = super.receive orElse {
	      case AskerAtOfferer(askerId,offererId,askerLat,askerLon) => {
	        if(flagNotify){
	        	flagNotify = false
	        	if(placesLeft>0){
			        Logger.info(
			            "User " + 
			            offererId + 
			            " is being notified that user " +
			            askerId + 
			            " wants to be picked"
			        )
			        notificationReceived += (askerId -> AskerNotification(askerId,askerLat,askerLon))
			        sender ! NotificationAskerAt(offererId,askerId,(askerLat*pow(10,6)).toInt,(askerLon*pow(10,6)).toInt)
	        	}
        	}else{
	        	Logger.info(
		            "User " + 
		            offererId + 
		            " will be notified that user " +
		            askerId + 
		            " wants to be picked once they reply to the pending notification"
		        )
		        notificationReceived += (askerId -> AskerNotification(askerId,askerLat,askerLon))
	        }
	      }
	      case AcceptedAskerOfferer(uid,askerId) => {
	        flagNotify = true
	        notificationReceived -= askerId
	        placesLeft -= 1
	        allowedUsers += askerId
	        Logger.info(
	            "User " + 
	            uid + 
	            " accepted user " +
	            askerId + 
	            ", they are being notified"
	        )
	        sender ! Route(askerId,AcceptedAskerAsker(uid,askerId))
	        processNextNotification
	        if(!(placesLeft>0))
	          removeRide
	      }
	      case RefusedAskerOfferer(uid,askerId) => {
	        flagNotify = true
	        notificationReceived -= askerId
	        Logger.info(
	            "User " + 
	            uid + 
	            " refused user " +
	            askerId + 
	            ", they are being notified"
	        )
	        sender ! Route(askerId,RefusedAskerAsker(uid,askerId))
	        processNextNotification
	      }
	      case StoreRide(userId,originLat,originLon,destLat,destLon,departureDate,nbPlaces,waypoints) => {
	        Logger.info("User " + userId + " is storing a ride from (" + originLat + "," + originLon + ") to (" + destLat + "," + destLon + ")")
	        placesLeft = nbPlaces
	        println(StoreRide(userId,originLat,originLon,destLat,destLon,departureDate,nbPlaces,waypoints))
	        storeInRedis(userId,originLat,originLon,destLat,destLon,departureDate.getTime,nbPlaces,waypoints)
	      }
		}
		
		/**
		 * Called to ease the one-by-one
		 * notification system
		 */
		def processNextNotification = {
			if(notificationReceived.size>0){
	          self ! AskerAtOfferer(notificationReceived.head._2.askerId,subj.thisuid,notificationReceived.head._2.askerLat,notificationReceived.head._2.askerLon)
	        }
		}
		
		/**
		 * Called when the ride becomes
		 * obsolete: places left = 0
		 */
		def removeRide = {
		  SearchHandler.removeFromIndex(subj.thisuid)
		  removeFromRedis(subj.thisuid)
		}
	}
	
	/**
	 * Handles the asker aspect of an online user, to be mixed-in with OnlineUser
	 */
	trait Asker extends UserType {
	  subj: OnlineUser => 
		abstract override def receive = super.receive orElse {
		  case AskerAtAsker(askerId,offererid,askerLat,askerLon) => {
		    Logger.info(
		        "User " + 
		        askerId + 
		        " choose the ride offered by " + 
		        offererid +
		        ", the notification is being routed"
		    )
	        sender ! Route(offererid, AskerAtOfferer(askerId,offererid,askerLat,askerLon))
	      }
		  case AcceptedAskerAsker(offererId,uid) => {
			allowedUsers += offererId
			sender ! NotificationAccepted(subj.thisuid,offererId)
		  }
		  case RefusedAskerAsker(offererId,uid) => {
			sender ! NotificationRefused(subj.thisuid,offererId)
		  }
		} 
	}
	
	/**
	 * Allows the stackability trick
	 */
	abstract class UserType extends Actor with MemoryCalls {
		def receive: PartialFunction[Any,Unit]
	}
	
	/**
	 * Simplifies notification storage
	 */
	case class AskerNotification(askerId: Long, askerLat: Double,askerLon: Double)
	
}