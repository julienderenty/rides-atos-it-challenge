package backend

import play.api.libs.json._

object ErrorHandling {
	def errorMsg(msg: String) = JsObject(Seq("type" -> JsString("error"),"error" -> JsString("Something weird happened: " + msg)))
}