package backend

import play.api.libs.json.JsObject
import play.api.libs.iteratee._
import play.api.libs.json._
import java.util.Date

object Messages {
	
  	case class IncomingEvent(uid: Long, event: JsValue)
	case class SearchForRides(userId:Long,originLat:Double,originLon:Double,destLat:Double,destLon:Double)
  
	/**
	 * Messages sent at user login and logout
	 * to the backend of rides in order
	 * to create or kill OnlineUser actor
	 */
	abstract class MessageToOnlineUserMaster
	case class CreateOnlineUser(uid: Long) extends MessageToOnlineUserMaster
	case class KillOnlineUser(uid: Long) extends MessageToOnlineUserMaster
	
	/**
	 * Incoming messages to be routed to
	 * OnlineUser actor to handle different
	 * situations
	 */
	abstract class MessageToOnlineUser(id: Long){
		val uid = id
	}
	case class UpdatedPosition(userId:Long,lat:Double,lon:Double) extends MessageToOnlineUser(userId)
	case class AskerAtOfferer(askerId:Long,userId:Long,askerLat:Double,askerLon:Double) extends MessageToOnlineUser(askerId)
	case class AskerAtAsker(askerId:Long,userId:Long,askerLat:Double,askerLon:Double) extends MessageToOnlineUser(askerId)
	case class AcceptedAskerOfferer(userId:Long,askerId:Long) extends MessageToOnlineUser(userId)
	case class AcceptedAskerAsker(userId:Long,askerId:Long) extends MessageToOnlineUser(userId)
	case class RefusedAskerOfferer(userId:Long,askerId:Long) extends MessageToOnlineUser(userId)
	case class RefusedAskerAsker(userId:Long,askerId:Long) extends MessageToOnlineUser(userId)
	case class StoreRide(userId:Long,originLat:Double,originLon:Double,destLat:Double,destLon:Double,arrivalDate:Date,nbPlaces:Int,wayPoints:List[(Double,Double)]) extends MessageToOnlineUser(userId)
	case class Route(id:Long,msg:MessageToOnlineUser)
	
	/**
	 * Messages to be exchanged
	 * between ServerConnection and OutgoingEventsHandler
	 * to establish Websocket connection and
	 * Enumerator tracking
	 */
	abstract class ServerConnectionToOutgoingEventsHandler
	case class LoggedIn(uid:Long) extends ServerConnectionToOutgoingEventsHandler
	case class LoggedOut(uid:Long) extends ServerConnectionToOutgoingEventsHandler
	case class CloseConnection(uid:Long) extends ServerConnectionToOutgoingEventsHandler
	
	abstract class OutgoingEventsHandlerToServerConnection
	case class Connected(enumerator:Enumerator[JsValue]) extends OutgoingEventsHandlerToServerConnection
	case class CannotConnect(msg: String) extends OutgoingEventsHandlerToServerConnection
	
	/**
	 * Messages to be sent to Outgoing events handler
	 * from OnlineUser
	 */
	abstract class OnlineUserToOutgoingEventsHandler(id: Long){
		val uid = id
	}
	case class UpdatePositionTic(userId:Long,pinId:Long,lat:Int,lon:Int) extends OnlineUserToOutgoingEventsHandler(userId)
	case class NotificationAskerAt(userId:Long,askerId:Long,askerLat:Int,askerLon:Int) extends OnlineUserToOutgoingEventsHandler(userId)
	case class NotificationAccepted(userId:Long,offererId:Long) extends OnlineUserToOutgoingEventsHandler(userId)
	case class NotificationRefused(userId:Long,offererId:Long) extends OnlineUserToOutgoingEventsHandler(userId)
	
	abstract class OnlineUserToHttpResponse
	case class SearchJsonMsg(msg:JsObject) extends OnlineUserToHttpResponse
	
}