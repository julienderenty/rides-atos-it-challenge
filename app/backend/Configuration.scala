package backend

import akka.util.Timeout
import akka.util.duration._
import akka.actor._
import net.fyrie.redis._

object Configuration {
	implicit val timeout = Timeout(30 second)
	implicit val system = ActorSystem("RidesPool")
	/**
	 * Redis configuration
	 */
	val config = RedisClientConfig()
	implicit val dispatcher = system.dispatcher
	val redisClient = new RedisClient("127.0.0.1", 6379, config)(system)
}