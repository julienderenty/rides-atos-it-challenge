package backend

import akka.actor._
import Configuration._
import OnlineUsersHandler._
import OnlineUserTraits._

object Actors {
	val onlineUserMaster = system.actorOf(Props(new OnlineUserMaster), "onlineUserMaster")
}