package backend

import akka.actor._
import OnlineUserTraits.UserType
import Messages._
import OnlineUserTraits._
import play.api.libs.json._
import scala.collection.mutable.ListBuffer
import play.api._
import akka.pattern._
import akka.util.Timeout
import akka.util.duration._
import play.api.libs.concurrent._
import play.api.Play.current
import scala.math._

object OnlineUsersHandler {
	
  implicit val timeout = new Timeout(1000)
	/**
	 * Responsible for all OnlineUser actors
	 * and for routing different messages
	 */
	class OnlineUserMaster extends Actor {
		def receive = {
		  case CreateOnlineUser(uid) => {
			  Logger.info("Creating online user... " + uid)
			  context.actorOf(Props(new OnlineUser(uid) with Offerer with Asker), uid.toString)
		  }
		  case KillOnlineUser(uid) => {
			  Logger.info("Killing online user... " + uid)
			  context.stop(context.actorFor(uid.toString))
		  }
		  case Route(askerId,msg) => context.actorFor(askerId.toString) ! msg
		  case msg: OnlineUserToOutgoingEventsHandler => backendInterface.OutgoingEventsInterface.outgoingEventsHandler ! msg
		  case msg: MessageToOnlineUser => context.actorFor(msg.uid.toString) ! msg
		  case other => Logger.error("Unexpected message received by OnlineUserMaster: " + other)
		}
	}
  
	/**
	 * Follows the actions
	 * of an online user
	 */
	class OnlineUser(thisui: Long) extends UserType {
		val thisuid = thisui
		var currentLat = -1000: Double
		var currentLon = -1000: Double
		var allowedUsers = ListBuffer[Long]()
		def receive = {
		  case UpdatedPosition(uid,lat,lon) => {
		    if(uid==thisuid){
		    	currentLat = lat
    			currentLon = lon
    			Logger.info("User " + uid + " has updated their position to (" + currentLat + "," + currentLon + ")")
    			allowedUsers.map({id => sender ! Route(id,UpdatedPosition(uid,lat,lon))})
		    }else{
		    	Logger.info("User " + thisuid + " is being notified that user " + uid + " has updated their position")
		    	sender ! UpdatePositionTic(thisuid,uid,(lat*pow(10,6)).toInt,(lat*pow(10,6)).toInt)
		    }
	      }
		}
	}
	
}